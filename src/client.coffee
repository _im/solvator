# socket = io()
# console.log socket
Player = {}
CurrentPart = null
IdleWatcher = null
Current = ""
LastTimePartEnded = -1
Calling = false

Looper = false

KickTime = 60 * 1000
StartMenu = "_"

Map = {}

History = null

numbers = []
signalsample = new Tone.Buffer("./samples/_signal.wav")
backsample = new Tone.Buffer("./samples/vissza.wav")

loadedBuffer = () ->
  $("#time").html ($("#time").html() + ".")

urlExists = (url) ->
  http = new XMLHttpRequest()
  http.open('HEAD', url, false)
  http.send()
  http.status isnt 404

newBuffer = (url) ->
  if urlExists url then new Tone.Buffer(url, loadedBuffer)
  else new Tone.Buffer("./samples/_empty.wav")

class Menu
  constructor : (params) ->
    @name = params[0]
    @parent = params[1]
    @outputs = params[2]
    @info = params[3]
    @getOutput = (i) -> if i < @outputs.length then @outputs[i] else null
    @info_buffer = newBuffer("./samples/"+@name+"_info.wav")
    @content_buffer = newBuffer("./samples/"+@name+".wav")
  toString : () ->
    outs = 
      for o, i in @outputs
        "#{i + 1} : #{Map[o].info}"
    @info + "\n" + outs.join "\n"


now = (time) -> 
  if time then new Date(time).getTime() 
  else new Date().getTime()

ClearCurrenPart = () ->
  Player.stop()
  CurrentPart.removeAll()
  # CurrentPart.dispose()

play = (node, nointro) ->
  if node.outputs.length is 0
    # DEAD END
    buffer = node.content_buffer
    # if node.name is "m_kapcsolat" then pickRandom

    CurrentPart = new Tone.Part(
      ((t, o) ->
        if !o.buffer?
          switchTo node.parent
        else
          Player.buffer = o.buffer
          Player.start()
      ),
      [
        {time:0, buffer:buffer}, 
        {time:buffer.duration}
      ]
    )
    CurrentPart.start()
  else
    # CLASSIC
    addDurations = (_a, _v) -> _a + _v.buffer.duration
    timetable = node.outputs.reduce(
      ((list, output, i) ->
        list.concat([
          {
            time : list.reduce(addDurations, 0)
            buffer : Map[output].info_buffer
          }
          {
            time : list.reduce(addDurations, 0) + Map[output].info_buffer.duration
            buffer : numbers[i + 1]
          }
          ]
        )
      ), (if nointro then [] else [{time : 0, buffer : node.content_buffer}] ))
    if node.name != "_"
      timetable.push({time : timetable.reduce(addDurations, 0), buffer : backsample})
    last = timetable[timetable.length - 1]
    ending_time = last.time + last.buffer.duration
    CurrentPart = new Tone.Part(
      ((t, o) ->
        if !o.buffer?
          LastTimePartEnded = now()
        else
          Player.buffer = o.buffer
          Player.start()
      ), 
      timetable.concat([{time:ending_time}]) 
    )
    CurrentPart.start()


watchIdle = () ->
  millisToSec = (ms) -> Math.abs(Math.floor(0.001 * ms)) + "s"
  t = ""
  if not Calling and History?
    t = "last call was " + millisToSec(now() - now(History.end_time)) + " ago"
  else if Calling
    et = millisToSec(now() - now(History.start_time))
    t = "call time : #{et}"
  if LastTimePartEnded isnt -1
    kt = millisToSec(now() - (LastTimePartEnded + KickTime) )
    t = t + "  --- hanging up in : #{kt}"
    if now() > LastTimePartEnded + KickTime
      hangup()
  $("#time").html t


pickup = () ->
  $("#call").css "display", "none"
  if not Calling
    Player.context.resume()
    console.log "picking up the phone"
    History = {start_time : new Date().toString(), actions:[]}
    LastTimePartEnded = -1
    Calling = true
    switchTo StartMenu, false
    
    # console.log "playing signal"
    # Player.buffer = signalsample
    # Player.start()
    # setTimeout((() -> switchTo(StartMenu, false)), signalsample.duration * 1000)

  # else
  #   hangup()
  #   pickup()

hangup = () ->
  History.end_time = new Date().toString()
  # socket.emit("hang_up", JSON.stringify History )
  # window.clearInterval IdleWatcher
  Player.stop()
  Tone.Transport.seconds = 0
  if isLooper Current
    Looper.stop()
  Current = ""
  Calling = false
  ClearCurrenPart()
  LastTimePartEnded = -1
  $("#call").css "display", "block"
  $("#screen").html "hanged up. \npress CALL to start a new call"

incomingdigit = (d) ->
  if Calling
    LastTimePartEnded = now()
    action = {digit : d}
    switch d
      when 'C' then hangup()
      when '*' then switchTo Map[Current].parent, true
      when '#' then # repeat menu items
      when '0','1','2','3','4','5','6','7','8','9'
        if Current? and isLooper Current
          Looper.toggle parseInt(d)
        else
          next = Map[Current].getOutput (if d is 0 then 0 else d - 1)
          if next?
            switchTo next, false
  else
    if d is 'A'
      pickup()

switchTo = (next, nointro) ->

  # console.log "entering #{next}"
  $("#screen").html Map[next].toString()
  Tone.Transport.seconds = 0
  LastTimePartEnded = -1
  if CurrentPart?
    ClearCurrenPart()

  if Current? and isLooper Current
    Looper.stop()

  if isLooper next
    LastTimePartEnded = now()
    Looper.init(parseInt next.substr(6,1))
  else
    play Map[next], nointro
  Current = next
  # History.actions.push next

load = () ->
  for i in [0..9]
    numbers.push newBuffer("./samples/#{i}.wav")
  for d in data
    Map[d[0]] = new Menu d
  Looper.load()
  Tone.Buffer.on("load", init)

decode = () ->
  Decoder.start()
  decoderdigit = (d) ->
    console.log "decoder got #{d}"
    incomingdigit(d)
  Decoder.callback = decoderdigit

class Track
  constructor : () ->
    @player = new Tone.Player(loop : true, volume : -6).connect Tone.Master
  toggle : () -> @player.mute = !@player.mute

isLooper = (c) -> c.substr(0, 6) is "looper"

Looper = 
  tracks : [0..8].map((i) -> new Track())
  songs : []
  current : 0
  load : () ->
    for s in [1..2]
      @songs.push(
        for t in [1..9]
          newBuffer("./samples/looper/#{s}/#{t}.wav")
      ) 
  start : () -> @tracks.map((t) -> t.player.start())
  stop : () -> @tracks.map((t) -> t.player.stop())

  init : (s) ->
    console.log s
    for t, i in @tracks
      t.player.buffer = @songs[s - ]1[i]
      t.player.mute = i isnt 0
    Looper.start()
  toggle : (i) ->
    if 0 < i < @tracks.length + 1
      @tracks[i - 1].toggle()      

init = () ->
  $("#phone").css "display", "flex"
  $("#time", "#screen").html ""
  IdleWatcher = window.setInterval(watchIdle, 1000)
  Player = new Tone.Player().toMaster()
  console.log "loaded"
  Tone.Transport.start()
  $(window).keydown((e) ->
    console.log e.key
    if "0123456789#*abcd".indexOf(e.key.toLowerCase()) isnt -1
      incomingdigit e.key
  )
  $(".numpad").click((e)-> 
    encode(e.currentTarget.innerHTML)
    incomingdigit(e.currentTarget.innerHTML)
  )

  $("#call").click pickup

  
  # decode()

  # socket.on("pick_up", pickup)

load()
