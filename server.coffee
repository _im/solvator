fs = require 'fs'
ip = require 'ip'
_ = require 'lodash'
express = require 'express'
# config = require("./config.js")
app = express()
url = require 'url'
http = require('http').Server(app)
io = require('socket.io')(http)
port = 8000

app.use(express.static(__dirname + "/"))

http.listen(port, () -> console.log("-> :#{port}"))

historycount = fs.readdirSync("./history").length

pickup = () ->
  console.log "send pickup to arduino"
  io.emit("pick_up")

hangup = (history) ->
  console.log "send hangup to arduino"
  historycount += 1
  H = JSON.parse history
  H.caller = historycount
  fs.writeFile("./history/call#{historycount}.json", JSON.stringify(H, null, 2))

connection = (socket) ->
  console.log socket.id + " joined."
  io.emit("samples", fs.readdirSync("./samples"))
  socket.on("incoming_call", pickup)
  socket.on("hang_up", hangup)
  socket.on('disconnect', () ->
  )
  socket.on('leave', (t) -> socket.disconnect())

io.on('connection', connection )
